import Life from './life'

document.addEventListener('DOMContentLoaded', function(){
    let life = new Life('canvas');
    life.init();
});