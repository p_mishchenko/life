export default class Life {
    constructor (id) {
        this.canvasId = id;

        //additional buttons and inputs
        this.cyclesInputId = 'cycles-count';
        this.btnStartId = 'start-btn';
        this.btnRestartId = 'restart-btn';

        this.storage = [];

        //in pixels
        this.cellSize = 10;

        this.minNeighborsCount = 2;
        this.maxNeighborsCount = 3;
    }

    init() {
        this.renderAdditionalElements();
        this.initAdditionalElements();
        this.setTableSizes();
        this.setEventListeners();
        this.fillEmptyTable();
    }

    setTableSizes() {
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.cellsWidthCount = this.getCellNumber(this.canvas.width);
        this.cellsHeightCount = this.getCellNumber(this.canvas.height);
    }

    renderAdditionalElements() {
        let div = document.createElement('div');
        div.innerHTML += '<input type="number" id="' + this.cyclesInputId + '" placeholder="Set count of cycles for generation...">\n' +
            '<button id="' + this.btnStartId + '">Start Life!</button>' +
            '<button id="' + this.btnRestartId + '">Restart</button>';
        document.body.appendChild(div);
    }

    initAdditionalElements() {
        this.canvas = document.getElementById(this.canvasId);
        this.ctx = this.canvas.getContext('2d');
        this.cyclesInput = document.getElementById(this.cyclesInputId);
    }

    setEventListeners() {
        let self = this;
        document.getElementById(self.btnStartId).addEventListener('click', function () {return self.startLifeCycles()});
        document.getElementById(self.btnRestartId).addEventListener('click', function () {return self.restart()});
        self.canvas.addEventListener('click', function () {return self.clickOnCanvas(event)});
    }

    clickOnCanvas(event) {
        this.toggleCellInStorage(event.offsetX, event.offsetY);
        this.drawField();
    }

    getCyclesCount() {
        let count = parseInt(this.cyclesInput.value);
        if (!count || (count <= 0)) {
            count = 1;
            this.setCyclesCount(count);
        }

        return count;
    }

    setCyclesCount(count) {
        this.cyclesInput.value  = count;
    }

    toggleCellInStorage(x, y) {
        this.storage[this.getCellNumber(y)][this.getCellNumber(x)] = !this.storage[this.getCellNumber(y)][this.getCellNumber(x)] ? 1 : 0;
    }

    getCellNumber(coordinate) {
        return Math.floor(coordinate / this.cellSize);
    }

    isCellAlive(x, y) {
        return this.storage[x][y] === 1;
    }

    drawField() {
        this.clearTable();

        for (let i = 0; i < this.cellsHeightCount; i++){
            for (let j = 0; j < this.cellsWidthCount; j++){
                if (this.isCellAlive(i, j)) {
                    this.fillCellInTable(j, i);
                }
            }
        }
    }

    fillEmptyTable() {
        this.storage = new Array(this.cellsHeightCount).fill(0)
            .map(row => new Array(this.cellsWidthCount).fill(0));
    }

    clearTable() {
        this.ctx.clearRect(0, 0, this.width, this.height);
    }

    fillCellInTable(x, y) {
        this.ctx.fillRect(x * this.cellSize, y * this.cellSize, this.cellSize, this.cellSize);
    }

    clearCyclesInput() {
        this.cyclesInput.value = 1;
    }

    countNeighbors(i, j) {
        let self = this;
        let neighbors = 0;
        const neighborsMatrix = [
            {x: -1, y: -1},
            {x: -1, y: 0},
            {x: -1, y: 1},
            {x: 0, y: -1},
            {x: 0, y: 1},
            {x: 1, y: -1},
            {x: 1, y: 0},
            {x: 1, y: 1},
        ];

        neighborsMatrix.forEach(function(neighbor) {
            if (self.isCellAlive(
                self.getCorrectCellByBorders(i, neighbor.x) + neighbor.x,
                self.getCorrectCellByBorders(j, neighbor.y) + neighbor.y
            )) {
                neighbors++;
            }

        });

        return neighbors;
    }

    startLifeCycles() {
        let cyclesCount = this.getCyclesCount();
        for (let i = 0; i < cyclesCount; i++) {
            this.startLife();
        }
    }

    restart() {
        this.fillEmptyTable();
        this.clearTable();
        this.clearCyclesInput();
    }

    startLife() {
        this.reInitStorage();
        this.drawField();
    }

    reInitStorage() {
        let tmpStorage = [];
        for (let i = 0; i < this.cellsHeightCount; i++){
            tmpStorage[i] = [];
            for (let j = 0; j < this.cellsWidthCount; j++) {
                tmpStorage[i][j] = this.checkCellIsAlive(i, j);
            }
        }

        this.storage = tmpStorage;
    }

    checkCellIsAlive(x, y) {
        let neighbors = this.countNeighbors(x, y);
        let isCellAlive = this.isCellAlive(x, y);
        let toggleCell = false;

        if ((!isCellAlive && (neighbors === this.maxNeighborsCount)) ||
            (isCellAlive && (neighbors < this.minNeighborsCount || neighbors > this.maxNeighborsCount))) {
            toggleCell = true;
        }

        return toggleCell ? +!isCellAlive : +isCellAlive;
    }

    getCorrectCellByBorders(coordinate, correction) {
        if ((correction < 0) && !coordinate) {
            //up to top
            return this.cellsHeightCount;
        } else if ((correction > 0) && (coordinate === this.cellsWidthCount - 1)) {
            //down
            return -1;
        } else {
            return coordinate;
        }
    }
}